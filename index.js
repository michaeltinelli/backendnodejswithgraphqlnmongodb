require('dotenv').config()
const { username, password } = require('./.env')
const express = require('express')
const cors =  require('cors')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const ExpressGraphQL = require('express-graphql')
const mongoConnect = `mongodb+srv://${username}:${password}@cluster0-lhlvj.mongodb.net/mydebts
?retryWrites=true&w=majority
`
mongoose.connect(mongoConnect, { useUnifiedTopology: true, useNewUrlParser: true })

const schemaProQM = require('./src/queryAndMutations/Profile')
const schemaPerQM = require('./src/queryAndMutations/Person')
const schemaProdQM = require('./src/queryAndMutations/Product')
const schemaPhoQM = require('./src/queryAndMutations/Photo')

const server = express()
server.use(express.static('public'))
server.use(bodyParser.json())
server.use(bodyParser.urlencoded({ extended: true }))
server.use(express.json())
server.use(cors())


server.use(`/profile`, ExpressGraphQL({
    schema: schemaProQM,
    graphiql: true,
}))

server.use(`/person`, ExpressGraphQL({
    schema: schemaPerQM,
    graphiql: true,
}))

server.use(`/product`, ExpressGraphQL({
    schema: schemaProdQM,
    graphiql: true,
}))

server.use(`/photo`, ExpressGraphQL({
    schema: schemaPhoQM,
    graphiql: true,
}))



server.listen(8500, () => {
    console.log(`running`)
})
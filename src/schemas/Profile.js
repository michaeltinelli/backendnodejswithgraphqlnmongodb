const {
    GraphQLID,
    GraphQLString,
    GraphQLNonNull,
    GraphQLObjectType,
} = require("graphql");

const ProfileType = new GraphQLObjectType({
    name: 'Profile',
    fields: () =>  ({
        _id: {  type: GraphQLID },
        name: { type: GraphQLString },
    })
})

module.exports = ProfileType
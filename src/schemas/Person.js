const {
    GraphQLID,
    GraphQLString,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLInt,
} = require("graphql");

const AddressType = require('./Address')
const ProfileType = require('./Profile')
const ProductType = require('./Product')

const PersonType = new GraphQLObjectType({
    name: 'Person',
    fields: () =>  ({
        _id: {  type: GraphQLID },
        name: { type: GraphQLString },
        last_name: { type: GraphQLString },
        email: { type: GraphQLString },
        password: { type: GraphQLString },
        phone: { type: GraphQLString },
        profiles: { type: new GraphQLList(ProfileType) },
        products: { type: new GraphQLList(ProductType) },
        address: { type:  AddressType },
    })
})

module.exports = PersonType
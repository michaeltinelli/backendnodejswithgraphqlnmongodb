const {
    GraphQLID,
    GraphQLString,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLInt,
} = require("graphql");

const MessageType = new GraphQLObjectType({
    name: 'Message',
    fields: () =>  ({
        message: { type: GraphQLString },
    })
})

module.exports = MessageType
const { GraphQLInputObjectType, GraphQLNonNull, 
    GraphQLString, GraphQLFloat, GraphQLObjectType } = require('graphql')

    const InputProduct = new GraphQLInputObjectType({
        name: 'InputProduct',
        fields: () => ({
            name: { type: new GraphQLNonNull(GraphQLString) },
            price: { type: new GraphQLNonNull(GraphQLFloat) },
        })
    })

    const FilterProduct = new GraphQLInputObjectType({
        name: 'FilterProduct',
        fields: () => ({
            _id: { type: new GraphQLNonNull(GraphQLString) }
        })
    })

    const UpdateProduct = new GraphQLInputObjectType({
        name: 'UpdateProduct',
        fields: () => ({
            name: { type: new GraphQLNonNull(GraphQLString) },
            price: { type: new GraphQLNonNull(GraphQLFloat) },
        })
    })
    

module.exports = { InputProduct, FilterProduct, UpdateProduct}
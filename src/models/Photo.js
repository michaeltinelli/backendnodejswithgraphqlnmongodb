const mongoose = require('mongoose')

const PhotoSchema = new mongoose.Schema({
    fullPath: String,
    person: { type: mongoose.Schema.Types.ObjectId, ref: 'Person' }
})

module.exports = mongoose.model('Photo', PhotoSchema)
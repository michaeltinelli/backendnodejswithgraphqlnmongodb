require('dotenv').config()
const { ApolloServer } = require('apollo-server')
const { importSchema } = require('graphql-import')
const cors =  require('cors')
const bodyParser = require('body-parser')

const schemasPath = './schemas/index.graphql'
const resolvers = require('./src/resolvers')

const mongoConnect = 'mongodb+srv://michaelrtinelli@gmail.com:redhot22@cluster0-kbydf.mongodb.net/test?retryWrites=true&w=majority'

const server = new ApolloServer({
    typeDefs: importSchema(schemasPath),
    resolvers
})

server.use(cors())
server.use(bodyParser.json())
server.use(bodyParser.urlencoded())

server.listen(8500, ({ url }) => {
    console.log(`running on ${url}`)
})


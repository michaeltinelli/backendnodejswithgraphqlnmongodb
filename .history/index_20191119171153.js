const express = require('express')
const cors =  require('cors')
const bodyParser = require('body-parser')

const app = express()
const port = 8455


app.listen(port, () => {
    console.log(`App is running on port: ${port}`)
})
require('dotenv').config()
const express = require('express')
const cors =  require('cors')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const ExpressGraphQL = require('express-graphql')
const mongoConnect = `mongodb+srv://tinelli22:redhot22@cluster0-lhlvj.mongodb.net/test
?retryWrites=true&w=majority
`
const { schemaQM } = require('./src/queryAndMutations/Profile')
const MongoClient = require('mongodb').MongoClient

//const client = new MongoClient(mongoConnect, { useNewUrlParser: true, useUnifiedTopology: true })
//client.connect((err) => {
//    console.warn(err)
//})

mongoose.connect(mongoConnect, { useUnifiedTopology: true, useNewUrlParser: true })
   // .then(resp => console.warn(resp))

const server = express()
server.use(cors())
server.use(bodyParser.json())
//server.use(bodyParser.urlencoded())

server.use(`/profile`, ExpressGraphQL({
    schema: schemaQM,
    graphiql: true,
}))

server.listen(8500, () => {
    console.log(`running`)
})
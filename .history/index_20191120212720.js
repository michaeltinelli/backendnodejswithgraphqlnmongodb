require('dotenv').config()
const express = require('express')
const cors =  require('cors')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const ExpressGraphQL = require('express-graphql')
const mongoConnect = 'mongodb+srv://michaelrtinelli@gmail.com:redhot22@cluster0-kbydf.mongodb.net/test?retryWrites=true&w=majority'
const { querySchema, mutationSchema } = require('./src/queryAndMutations/Profile')

mongoose.connect(mongoConnect, {
    useUnifiedTopology: true,
    useNewUrlParser: true
})

const server = express()
server.use(cors())
server.use(bodyParser.json())
//server.use(bodyParser.urlencoded())

server.use(`/profile`, ExpressGraphQL({
    schema: [mutationSchema],
    graphiql: true,
}))

server.listen(8500, () => {
    console.log(`running`)
})


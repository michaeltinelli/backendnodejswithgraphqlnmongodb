const ProfileModel = require('../../models/Profile')

module.exports = {
    async compareProfile(name) {
        name = name.toUpperCase()
        return await ProfileModel.findOne({ name }).select()
    },
   
}
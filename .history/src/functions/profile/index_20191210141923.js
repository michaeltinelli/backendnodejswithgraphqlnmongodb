const ProfileModel = require('../../models/Profile')

module.exports = {
    async compareProfile(name) {
        return await ProfileModel.findOne({ name }).select()
    }
}
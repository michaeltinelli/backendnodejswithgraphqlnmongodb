const PersonModel = require('../../models/Person')

module.exports = {
    async compareEmail(email) {
        return await PersonModel.findOne({ email }).select({ email })
    },
    async getPersonById(_id) {
        return await PersonModel.findById({ _id }).select("_id")
    }
}
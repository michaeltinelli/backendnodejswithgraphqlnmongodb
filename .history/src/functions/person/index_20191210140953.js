const PersonModel = require('../../models/Person')

module.exports = {
    async compareEmail(email) {
        return await PersonModel.findOne({ email }).select({ email })
    }
}
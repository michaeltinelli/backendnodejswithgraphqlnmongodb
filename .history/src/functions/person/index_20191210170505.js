const PersonModel = require('../../models/Person')

module.exports = {
    async compareEmail(email) {
        return await PersonModel.findOne({ email }).select({ email })
    },
    async getPersonById(filter) {
        return await PersonModel.findOne({ ...filter}).select("_id")
    }
}
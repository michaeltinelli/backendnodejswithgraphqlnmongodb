const mongoose = require('mongoose')
const PhotoSchema= require('./Photo').schema

const ProductSchema = new mongoose.Schema({

    name: String,
    price: Number,
    createdAt: Date,
    updatedAt: Date,
    photos: [PhotoSchema]
})

module.exports = mongoose.model('Product', ProductSchema)
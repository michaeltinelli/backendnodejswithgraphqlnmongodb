const mongoose = require('mongoose')

const ProductSchema = new mongoose.Schema({

    name: String,
    price: Number,
    createdAt: Date,
    updatedAt: Date,
    person_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Person'},
})

module.exports = mongoose.model('Product', ProductSchema)
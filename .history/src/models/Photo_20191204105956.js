const mongoose = require('mongoose')

const PhotoSchema = new mongoose.Schema({
    url: String,
})

module.exports = mongoose.model('Photo', PhotoSchema)
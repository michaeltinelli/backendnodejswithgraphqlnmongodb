const mongoose = require('mongoose')

const PersonSchema = new mongoose.Schema({
    name: String,
    last_name: String,
    phone: String,
    email: String,
    password: String,
    
})

module.exports = mongoose.model('Person', PersonSchema)
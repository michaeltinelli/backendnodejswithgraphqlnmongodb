const mongoose = require('mongoose')

const AddressSchema = new mongoose.Schema({
    name: String,
    comp: String,
    cep: Int,
    num: Int,
})

module.exports = mongoose.model('Address', AddressSchema)
const mongoose = require('mongoose')

const AddressSchema = new mongoose.Schema({
    name: String,
    comp: String,
    cep: Number,
    num: Number,
})

module.exports = mongoose.model('Address', AddressSchema)
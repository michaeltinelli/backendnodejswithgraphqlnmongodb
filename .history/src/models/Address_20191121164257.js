const mongoose = require('mongoose')

const AddressSchema = new mongoose.Schema({
    name: String,
    comp: String,
    cep: mongoose.SchemaTypes.Number,
    num: mongoose.SchemaTypes.Number,
})

module.exports = mongoose.model('Address', AddressSchema)
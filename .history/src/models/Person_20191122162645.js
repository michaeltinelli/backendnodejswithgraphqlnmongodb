const mongoose = require('mongoose')
const Profile = require('./Profile')
const Address = require('./Address')

const PersonSchema = new mongoose.Schema({
    name: String,
    last_name: String,
    phone: String,
    email: String,
    password: String,
    profiles: [mongoose.SchemaTypes.ObjectId],
    address: mongoose.SchemaTypes.ObjectId
})

module.exports = mongoose.model('Person', PersonSchema)
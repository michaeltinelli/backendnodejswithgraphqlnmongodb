const mongoose = require('mongoose')
const Profile = require('./Profile').schema
const Address = require('./Address').schema

const PersonSchema = new mongoose.Schema({
    name: String,
    last_name: String,
    phone: String,
    email: String,
    password: String,
    profiles: [Profile],
    address: Address
})

module.exports = mongoose.model('Person', PersonSchema)
const mongoose = require('mongoose')

const PhotoSchema = new mongoose.Schema({
    fullPath: String,
})

module.exports = mongoose.model('Photo', PhotoSchema)
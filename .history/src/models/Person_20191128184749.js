const mongoose = require('mongoose')
const Profile = require('./Profile').schema
const Address = require('./Address').schema
const Product = require('./Product').schema

const PersonSchema = new mongoose.Schema({
    name: String,
    last_name: String,
    phone: String,
    email: String,
    password: String,
    isActive: {
        type: Boolean,
        default: true
    },
    profiles: [Profile],
    products: [Product],
    address: Address
})

module.exports = mongoose.model('Person', PersonSchema)
const mongoose = require('mongoose')
const Address = require('./Address').schema


const PersonSchema = new mongoose.Schema({
    name: String,
    last_name: String,
    phone: String,
    email: String,
    password: String,
    isActive: {
        type: Boolean,
        default: true
    },
    profiles: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Profile' }],
    address: Address
})

module.exports = mongoose.model('Person', PersonSchema)
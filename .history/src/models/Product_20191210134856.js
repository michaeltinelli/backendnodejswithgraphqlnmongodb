const mongoose = require('mongoose')
const PhotoSchema= require('./Photo').schema

const ProductSchema = new mongoose.Schema({

    name: String,
    price: Number,
    createdAt: Date,
    updatedAt: Date,
    person_Id: { type: mongoose.SchemaTypes.ObjectId, ref: 'Person'},
    photos: [PhotoSchema]
})

module.exports = mongoose.model('Product', ProductSchema)
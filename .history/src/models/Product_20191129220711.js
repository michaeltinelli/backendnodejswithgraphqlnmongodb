const mongoose = require('mongoose')

const ProductSchema = new mongoose.Schema({

    name: String,
    price: mongoose.SchemaTypes.Number,
    createdAt: Date,
    updatedAt: Date
})

module.exports = mongoose.model('Product', ProductSchema)
const mongoose = require('mongoose')

const ProfileSchema = new mongoose.Schema({
    name: String,
})

module.exports = mongoose.model('Profile', ProfileSchema)
const mongoose = require('mongoose')
const Profile = require('./Profile')
const Address = require('./Address')

const PersonSchema = new mongoose.Schema({
    name: String,
    last_name: String,
    phone: String,
    email: String,
    password: String,
    profiles: mongoose.SchemaTypes.Array,
    address: mongoose.SchemaTypes.Mixed
})

module.exports = mongoose.model('Person', PersonSchema)
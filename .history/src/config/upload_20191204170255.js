const multer = require('multer')
const path = require('path')
const uuid = require('uuid/v1')
const fs_extra = require('fs-extra')

module.exports = {
    stProduct: multer.diskStorage({
        destination: (req, file, cb) => {
            
            const { _idProduct } = req.body
            const { _id } = req.headers
            
            const dirUser = path.resolve(__dirname, '..', 'images', `${_id}` , `product_${_idProduct}`)
            cb(null, dirUser)

        },
    })
}
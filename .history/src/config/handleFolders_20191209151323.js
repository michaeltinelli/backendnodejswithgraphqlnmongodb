const fs_extra = require('fs-extra')
const path = require('path')

module.exports = {
    createFolderPerson(_id) {
        const dirUser = path.resolve(__dirname, '..', 'images', `${_id}`)
        fs_extra.ensureDirSync(dirUser)
    },
    deleteUserFolder(_id) {
        const dirUser = path.resolve(__dirname, '..', '..', 'images', `${_id}`)
        fs_extra.removeSync(dirUser)
    },
    
}
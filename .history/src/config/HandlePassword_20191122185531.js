const bcrypt = require('bcrypt')

module.exports = {
    async toHashPassword(password) {
        return await bcrypt.hashSync(password, 10)
    },
    async toComparePassword(password, pswdEncrypted) {
        return await bcrypt.compareSync(password, pswdEncrypted)
    }
}
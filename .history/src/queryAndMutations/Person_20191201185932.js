const { GraphQLSchema, GraphQLObjectType, GraphQLList, GraphQLNonNull, GraphQLString } = require('graphql')

const PersonType = require('../schemas/Person')
const MessageType = require('../schemas/Message')
const ProductType = require('../schemas/Product')

const { getPeople, addPerson, updatePerson, removePerson } = require('../resolvers/Person')
const { InputPerson, ArgsUpdatePerson, UpdatePerson,  } = require('../schemas/inputs/InputPerson')
const { InputProduct } = require('../schemas/inputs/inputProduct')

const schemaPQM = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: {
            people: {
                type: GraphQLList(PersonType),
                resolve: getPeople
            },



            //----------------------------------------[Product Area]----------------------------------------//
            
        }
    }),



    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: {
            person: {
                type: PersonType,
                args: { 
                   input: { type: InputPerson },
                },
                resolve: addPerson
            },
            updatePerson: {
                type: PersonType,
                args: {
                    filter: { type: ArgsUpdatePerson },
                    newsData: { type: UpdatePerson }
                },
                resolve: updatePerson
            },
            removePerson: {
                type: MessageType,
                args: {
                    filter: { type: ArgsUpdatePerson },
                },
                resolve: removePerson
            },


            //----------------------------------------[Product Area]----------------------------------------//
            addHisProduct: {
                type: ProductType,
                args: { 
                    input: { type: InputProduct }
                },
                resolve 
           },
        }
    }),
})


module.exports = schemaPQM
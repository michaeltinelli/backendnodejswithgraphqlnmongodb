const PersonType = require('../schemas/Person')
const { GraphQLSchema, GraphQLObjectType, GraphQLList, GraphQLNonNull, GraphQLString } = require('graphql')
const { getPeople, addPerson } = require('../resolvers/Person')
const InputPerson = require('../schemas/inputs/InputPerson')

const schemaPQM = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: {
            people: {
                type: GraphQLList(PersonType),
                resolve: getPeople
            },
        }
    }),

    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: {
            person: {
                type: PersonType,
                args: { 
                   person: { type: InputPerson }
                },
                resolve: addPerson
            }
        }
    }),
})


module.exports = schemaPQM
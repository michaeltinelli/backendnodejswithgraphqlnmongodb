const { GraphQLNonNull, GraphQLString } = require('graphql')
const { GraphQLSchema, GraphQLObjectType, GraphQLList } = require('graphql')
const { ProfileType } = require('../schemas/Profile')
const { addProfile, getProfiles, getProfile, removeProfile
 } = require('../resolvers/Profile')

const schemaQM = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: {
            profiles: {
                type: GraphQLList(ProfileType),
                resolve: getProfiles
            },
            profile: {
                type: ProfileType,
                args: { _id: { type: new GraphQLNonNull(GraphQLString) } },
                resolve: getProfile
            }
        }
    }),



    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: {
            profile: {
                type: ProfileType,
                args: {
                    name: { type: new GraphQLNonNull(GraphQLString) }
                },
                resolve: addProfile
            },
            removeProfile: {
                type: ProfileType,
                args: { _id: { type: GraphQLString }, name: { type: GraphQLString } },
                resolve: removeProfile
            }
        }
    }),
})


module.exports = {
    schemaQM,
}
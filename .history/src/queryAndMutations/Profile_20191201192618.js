const { GraphQLNonNull, GraphQLString } = require('graphql')
const { GraphQLSchema, GraphQLObjectType, GraphQLList } = require('graphql')

const  ProfileType  = require('../schemas/Profile')
const { InputProfile, FilterProfile } = require('../schemas/inputs/InputProfile')

const { addProfile, getProfiles, getProfile, removeProfile, updateProfile
 } = require('../resolvers/Profile')

const schemaQM = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: {
            profiles: {
                type: GraphQLList(ProfileType),
                resolve: getProfiles
            },
            profile: {
                type: ProfileType,
                args: {
                    filter: { type: FilterProfile }
                },
                resolve: getProfile
            },
        }
    }),



    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: {
            profile: {
                type: ProfileType,
                args: { input: { type: InputProfile } },
                resolve: addProfile
            },
            updateProfile: {
                type: ProfileType,
                args: { filter: { type: FilterProfile } },
                resolve: updateProfile
            }
        }
    }),
})


module.exports = schemaQM
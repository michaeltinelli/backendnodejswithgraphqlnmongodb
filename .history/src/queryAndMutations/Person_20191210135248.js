const { GraphQLSchema, GraphQLObjectType, GraphQLList, GraphQLNonNull, GraphQLString } = require('graphql')

const PersonType = require('../schemas/Person')
const MessageType = require('../schemas/Message')
const ProductType = require('../schemas/Product')

const { getPeople, addPerson, updatePerson, removePerson } = require('../resolvers/Person')
const { InputPerson, FilterPerson, UpdatePerson,  } = require('../schemas/inputs/InputPerson')

const { InputProduct, FilterProduct, UpdateProduct } = require('../schemas/inputs/inputProduct')
const { addHisProduct, getHisProducts, updateOneOfHisProducts, removeOneOfHisProduct } = require('../resolvers/Product')

const { InputPhoto, FilterPhoto } = require("../schemas/inputs/inputPhoto")
const { savePhotoOfProduct, removePhotoOfProduct } = require("../resolvers/Photo")

const schemaPQM = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: {
            people: {
                type: GraphQLList(PersonType),
                resolve: getPeople
            },
        }
    }),



    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: {
            person: {
                type: PersonType,
                args: { 
                   input: { type: InputPerson },
                },
                resolve: addPerson
            },
            updatePerson: {
                type: PersonType,
                args: {
                    filter: { type: FilterPerson },
                    newsData: { type: UpdatePerson }
                },
                resolve: updatePerson
            },
            removePerson: {
                type: MessageType,
                args: {
                    filter: { type: FilterPerson },
                },
                resolve: removePerson
            },
        }
    }),
})


module.exports = schemaPQM
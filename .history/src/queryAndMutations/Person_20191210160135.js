const { GraphQLSchema, GraphQLObjectType, GraphQLList, GraphQLNonNull, GraphQLString } = require('graphql')

const PersonType = require('../schemas/Person')
const MessageType = require('../schemas/Message')

const { getPeople, addPerson, updatePerson, removePerson, getPerson } = require('../resolvers/Person')
const { InputPerson, FilterPerson, UpdatePerson, } = require('../schemas/inputs/InputPerson')

const schemaPQM = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: {
            people: {
                type: GraphQLList(PersonType),
                resolve: getPeople
            },
            person: {
                type: PersonType,
                args: {
                    filter: { type: FilterPerson }
                },
                resolve: getPerson
            }
        }
    }),



    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: {
            person: {
                type: PersonType,
                args: { 
                   input: { type: InputPerson },
                },
                resolve: addPerson
            },
            updatePerson: {
                type: PersonType,
                args: {
                    filter: { type: FilterPerson },
                    input: { type: UpdatePerson }
                },
                resolve: updatePerson
            },
            removePerson: {
                type: MessageType,
                args: {
                    filter: { type: FilterPerson },
                },
                resolve: removePerson
            },
        }
    }),
})


module.exports = schemaPQM
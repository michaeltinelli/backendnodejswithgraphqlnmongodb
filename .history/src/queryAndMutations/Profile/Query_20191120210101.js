import { GraphQLNonNull, GraphQLString } from 'graphql'
const { GraphQLSchema, GraphQLObjectType, GraphQLList } = require('graphql')
const { ProfileType } = require('../../schemas/Profile')
const { addProfile } = require('../../resolvers/Profile')

export const querySchema = GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: {
            profiles: {
                type: GraphQLList(ProfileType),
                
            }
        }
    }),

    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: {
            profile: {
                type: GraphQLObjectType(ProfileType),
                args: {
                    name: { type: new GraphQLNonNull(GraphQLString) }
                },
                resolve: addProfile
            }
        }
    }),
})
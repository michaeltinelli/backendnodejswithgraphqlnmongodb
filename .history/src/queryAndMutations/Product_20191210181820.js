const { GraphQLSchema, GraphQLObjectType, GraphQLList, GraphQLNonNull, GraphQLString } = require('graphql')
const ProductType = require('../schemas/Product')
const { InputProduct, FilterProduct } = require("../schemas/inputs/inputProduct")
const { FilterPerson, } = require("../schemas/inputs/InputPerson")
const { addProduct, getProductsOfPerson, updateProduct  } = require("../resolvers/Product")

const schemaProdQM = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: {
            getProdsByPerson: {
                type: GraphQLList(ProductType),
                args: {
                    filterPerson: { type: FilterPerson }
                },
                resolve: getProductsOfPerson
            }
        }
    }),



    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: {
            addProduct: {
                type: ProductType,
                args: {
                    input: { type: InputProduct },
                    filterPerson: { type: FilterPerson }
                },
                resolve: addProduct
            },
            updateProduct: {
                type: ProductType,
                args: {
                    input: { type: InputProduct },
                    filterPerson: { type: FilterPerson },
                    filterProduct: { type: FilterProduct }
                },
                resolve: updateProduct
            }
        }
    }),
})


module.exports = schemaProdQM
const { GraphQLSchema, GraphQLObjectType, GraphQLList, GraphQLNonNull, GraphQLString } = require('graphql')

const PersonType = require('../schemas/Person')
const MessageType = require('../schemas/Message')
const ProductType = require('../schemas/Product')

const { getPeople, addPerson, updatePerson, removePerson } = require('../resolvers/Person')
const { InputPerson, FilterPerson, UpdatePerson,  } = require('../schemas/inputs/InputPerson')

const { InputProduct, FilterProduct, UpdateProduct } = require('../schemas/inputs/inputProduct')
const { addHisProduct, getHisProducts, updateOneOfHisProducts } = require('../resolvers/Product')

const { InputPhoto } = require("../schemas/inputs/inputPhoto")
const { savePhotoOfProduct } = require("../resolvers/Photo")

const schemaPQM = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: {
            people: {
                type: GraphQLList(PersonType),
                resolve: getPeople
            },
            getHisProducts: {
                type: GraphQLList(ProductType),
                args: {
                    filter: { type: FilterPerson }
                },
                resolve: getHisProducts
            }


            //----------------------------------------[Product Area]----------------------------------------//
            
        }
    }),



    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: {
            person: {
                type: PersonType,
                args: { 
                   input: { type: InputPerson },
                },
                resolve: addPerson
            },
            updatePerson: {
                type: PersonType,
                args: {
                    filter: { type: FilterPerson },
                    newsData: { type: UpdatePerson }
                },
                resolve: updatePerson
            },
            removePerson: {
                type: MessageType,
                args: {
                    filter: { type: FilterPerson },
                },
                resolve: removePerson
            },


            //----------------------------------------[Product Area]----------------------------------------//
            addHisProduct: {
                type: ProductType,
                args: { 
                    input: { type: InputProduct }
                },
                resolve: addHisProduct 
           },
           updateOneOfHisProducts: {
                type: ProductType,
                args: {
                    filterPerson: { type: FilterPerson },
                    filterProduct: { type: FilterProduct },
                    product: { type: UpdateProduct }
                },
                resolve: updateOneOfHisProducts
           },
           addPhotosOfHisOneProduct: {
               type: MessageType,
               args: {
                    filterPerson: { type: FilterPerson },
                    filterProduct: { type: FilterProduct },
                    input: { type: InputPhoto }
               },
               resolve: savePhotoOfProduct
           }
        }
    }),
})


module.exports = schemaPQM
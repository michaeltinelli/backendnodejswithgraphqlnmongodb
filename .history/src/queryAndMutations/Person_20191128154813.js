const { GraphQLSchema, GraphQLObjectType, GraphQLList, GraphQLNonNull, GraphQLString } = require('graphql')

const PersonType = require('../schemas/Person')
const MessageType = require('../schemas/Message')

const { getPeople, addPerson, updatePerson, removePerson } = require('../resolvers/Person')
const { InputPerson, ArgsUpdatePerson, UpdatePerson,  } = require('../schemas/inputs/InputPerson')

const schemaPQM = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: {
            people: {
                type: GraphQLList(PersonType),
                resolve: getPeople
            },
        }
    }),

    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: {
            person: {
                type: PersonType,
                args: { 
                   person: { type: InputPerson },
                },
                resolve: addPerson
            },
            updatePerson: {
                type: PersonType,
                args: {
                    filter: { type: ArgsUpdatePerson },
                    newsData: { type: UpdatePerson }
                },
                resolve: updatePerson
            },
            removePerson: {
                type: MessageType,
                args: {
                    filter: { type: ArgsUpdatePerson },
                },
                resolve: removePerson
            }
        }
    }),
})


module.exports = schemaPQM
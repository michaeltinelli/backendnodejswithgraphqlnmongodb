const { GraphQLSchema, GraphQLObjectType, GraphQLList, GraphQLNonNull, GraphQLString } = require('graphql')
const ProductType = require('../schemas/Product')
const { InputProduct } = require("../schemas/inputs/inputProduct")
const { FilterPerson, } = require("../schemas/inputs/InputPerson")
const { addProduct, getProductsOfPerson  } = require("../resolvers/Product")

const schemaProdQM = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: {
            getProdsByPerson: {
                type: GraphQLList(ProductType),
                args: {
                    filterPerson: { type: FilterPerson }
                },
                resolve: getProductsOfPerson
            }
        }
    }),



    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: {
            addProduct: {
                type: ProductType,
                args: {
                    input: { type: InputProduct },
                    filterPerson: { type: FilterPerson }
                },
                resolve: addProduct
            }
        }
    }),
})


module.exports = schemaProdQM
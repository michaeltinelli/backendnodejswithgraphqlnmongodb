const PersonType = require('../schemas/Person')
const { GraphQLNonNull, GraphQLString } = require('graphql')
const { GraphQLSchema, GraphQLObjectType, GraphQLList } = require('graphql')

const schemaPersonQM = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: {
            people: {
                type: GraphQLList(PersonType),
                
            },
        }
    }),

    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: {
            personClient: {
                type: PersonType,
                args: {
                    name: { type: new GraphQLNonNull(GraphQLString) }
                },
                
            },
        }
    }),
})


module.exports = {
    schemaPersonQM,
}
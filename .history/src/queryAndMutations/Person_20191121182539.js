const PersonType = require('../schemas/Person')
const { GraphQLSchema, GraphQLObjectType, GraphQLList } = require('graphql')
const { getPerson } = require('../resolvers/Person')

const schemaPersonQM = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: {
            people: {
                type: GraphQLList(PersonType),
                resolve: getPerson
            },
        }
    }),

    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: {
            
        }
    }),
})


module.exports = {
    schemaPersonQM,
}
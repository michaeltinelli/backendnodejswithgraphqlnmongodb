const PersonType = require('../schemas/Person')
const ProfileType = require('../schemas/Profile')
const AddressType = require('../schemas/Address')

const { GraphQLSchema, GraphQLObjectType, GraphQLList, GraphQLNonNull, GraphQLString } = require('graphql')
const { getPeople, addPerson } = require('../resolvers/Person')

const schemaPQM = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: {
            people: {
                type: GraphQLList(PersonType),
                resolve: getPeople
            },
        }
    }),

    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: {
            person: {
                type: PersonType,
                args: { 
                   
                },
                resolve: addPerson
            }
        }
    }),
})


module.exports = schemaPQM
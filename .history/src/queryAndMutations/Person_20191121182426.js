const PersonType = require('../schemas/Person')
const { GraphQLSchema, GraphQLObjectType, GraphQLList } = require('graphql')
const { person } = require('../resolvers/Person')

const schemaPersonQM = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: {
            people: {
                type: new GraphQLList(PersonType),
                resolve: person
            },
        }
    }),

    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: {
            
        }
    }),
})


module.exports = {
    schemaPersonQM,
}
const { GraphQLNonNull, GraphQLString } = require('graphql')
const { GraphQLSchema, GraphQLObjectType, GraphQLList } = require('graphql')
const { ProfileType } = require('../schemas/Profile')
const { addProfile } = require('../resolvers/Profile')

const schemaQM = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: {
            profiles: {
                type: GraphQLList(ProfileType),
                
            }
        }
    }),



    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: {
            profile: {
                type: ProfileType,
                args: {
                    name: { type: new GraphQLNonNull(GraphQLString) }
                },
                resolve: addProfile
            }
        }
    }),
})


module.exports = {
    schemaQM,
}
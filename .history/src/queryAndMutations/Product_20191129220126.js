const { GraphQLNonNull, GraphQLString } = require('graphql')
const { GraphQLSchema, GraphQLObjectType, GraphQLList } = require('graphql')
const ProductType = require('../schemas/Product')
const { InputProduct } = require('../schemas/inputs/inputProduct')
const { addProduct, getProducts } = require('../resolvers/Produtcs')

const schemaQM = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: {
           products: {
               type: GraphQLList(ProductType),
               resolve: getProducts
           }
        }
    }),

    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: {
            product: {
                type: ProductType,
                args: { 
                    input: { type: InputProduct }
                },
                resolve: addProduct
           }
        }
    }),
})


module.exports = schemaQM
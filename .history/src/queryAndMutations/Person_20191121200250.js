const PersonType = require('../schemas/Person')
const ProfileType = require('../schemas/Profile')
const AddressType = require('../schemas/Address')

const { GraphQLSchema, GraphQLObjectType, GraphQLList, GraphQLNonNull, GraphQLString } = require('graphql')
const { getPeople, addPerson } = require('../resolvers/Person')

const schemaPQM = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: {
            people: {
                type: GraphQLList(PersonType),
                resolve: getPeople
            },
        }
    }),

    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: {
            person: {
                type: new GraphQLObjectType,
                args: { 
                    name: { type: new GraphQLNonNull(GraphQLString) }, 
                    last_name: { type: new GraphQLNonNull(GraphQLString) },
                    phone: { type: new GraphQLNonNull(GraphQLString) },
                    email: { type: new GraphQLNonNull(GraphQLString) },
                    password: { type: new GraphQLNonNull(GraphQLString) },
                    profiles: { type: new GraphQLNonNull(new GraphQLList(ProfileType)) },
                    address: { type: new GraphQLNonNull(AddressType) }    
                },
                resolve: addPerson
            }
        }
    }),
})


module.exports = schemaPQM
const { GraphQLSchema, GraphQLObjectType, GraphQLList, GraphQLNonNull, GraphQLString } = require('graphql')

const ProductType = require('../schemas/Product')
const MessaType = require("../schemas/Message")

const { FilterPerson, } = require("../schemas/inputs/InputPerson")

const { InputProduct, FilterProduct } = require("../schemas/inputs/inputProduct")
const { addProduct, getProductsOfPerson, updateProduct, removeProduct  } = require("../resolvers/Product")

const schemaProdQM = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: {
            getProdsByPerson: {
                type: GraphQLList(ProductType),
                args: {
                    filterPerson: { type: FilterPerson }
                },
                resolve: getProductsOfPerson
            }
        }
    }),



    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: {
            addProduct: {
                type: ProductType,
                args: {
                    input: { type: InputProduct },
                    filterPerson: { type: FilterPerson }
                },
                resolve: addProduct
            },
            updateProduct: {
                type: ProductType,
                args: {
                    input: { type: InputProduct },
                    filterPerson: { type: FilterPerson },
                    filterProduct: { type: FilterProduct }
                },
                resolve: updateProduct
            },
            removeProduct: {
                type: MessaType,
                args: {
                    filterProduct: { type: FilterProduct }
                },
                resolve: removeProduct
            }
        }
    }),
})


module.exports = schemaProdQM
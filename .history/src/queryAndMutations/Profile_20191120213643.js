const { GraphQLNonNull, GraphQLString } = require('graphql')
const { GraphQLSchema, GraphQLObjectType, GraphQLList } = require('graphql')
const { ProfileType } = require('../schemas/Profile')
const { addProfile } = require('../resolvers/Profile')

const querySchema = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: {
            profiles: {
                type: GraphQLList(ProfileType),
                
            }
        }
    }),
})

const mutationSchema = new GraphQLSchema({
   
    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: {
            profile: {
                type: GraphQLObjectType(ProfileType),
                args: {
                    name: { type: new GraphQLNonNull(GraphQLString) }
                },
                resolve: addProfile
            }
        }
    }),
})

module.exports = {
    querySchema,
    mutationSchema
}
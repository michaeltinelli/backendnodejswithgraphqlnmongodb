const PersonModel = require('../models/Person')
const { toHashPassword } = require('../config/handlePassword')
const { compareEmail } = require('../functions/person/index')
const { compareProfile } =  require('../functions/profile/index')

module.exports = {
    async addPerson(root, { input }, context) {
        console.log(input)
        const validEmail = await compareEmail(input.email)
        
        if(!validEmail) {
           
            const password = await toHashPassword(input.password)
            input.password = password
            

        } else {
            return new Error('Erro ao cadastrar.')
        }
    },
    async updatePerson(root, { newsData, filter }) {
        
    },
    async removePerson(root, { filter }) {
        const resp = await PersonModel.findOneAndRemove({ ...filter }).exec()

        if(resp == null) {
            return new Error("Filtros não encontrados.")
        } else {
            return { message: "Removído com sucesso." }
        }
    }
}
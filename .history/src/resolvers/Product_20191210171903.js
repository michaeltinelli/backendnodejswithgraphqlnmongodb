const PersonModel = require('../models/Person')
const ProductModel = require('../models/Product')
const { getPerson } = require("../functions/person/index")

module.exports = {
    async addProduct(root, { input, filterPerson }) {
        const { _id } = await getPerson(filterPerson)
        if(!_id) return new Error("Filtros inválidos.")
        console.log(_id)

        const prod = await new ProductModel({...input, person_id: { ...filterPerson } })
    },
    async getProductsOfPerson(root, { filterPerson }) {
        const _id = await getPerson(filterPerson)
        console.log(_id)
        if(_id) return new Error("Filtros inválidos")
        else {
            return await ProductModel.find({ person_id: _id })
        }
    },
    async updateProduct(root, { product, filterPerson, filterProduct }) {
        
        
    },
    async removeProduct(root, { filterPerson, filterProduct }) {
        
    }
}
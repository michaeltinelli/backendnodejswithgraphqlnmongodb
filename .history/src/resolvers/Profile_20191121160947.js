const ProfileModel = require('../models/Profile')

module.exports =  {

    async addProfile(root, { name }) {
        const profile = new ProfileModel({ name })
        const resp = await profile.save()
        console.warn(resp)
        return resp        
    },
    async getProfiles() {
        return await ProfileModel.find().exec()
    },
    async getProfile(root, { _id }) {
        return await ProfileModel.findById(_id).exec()
    },
    //não funcionando
    async removeProfile(root, args) {
        return await ProfileModel.findOneAndDelete({...args}).exec()
    },
    async updateProfile(root, { name, _id }) {
        
        await ProfileModel.updateOne({ _id }, { name }).exec()
        return  this.getProfile(root, _id)
    }
    
}
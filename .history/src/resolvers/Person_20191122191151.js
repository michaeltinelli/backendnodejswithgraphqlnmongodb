const PersonModel = require('../models/Person')
const { toHashPassword } = require('../config/HandlePassword')


function compareEmail(email) {
    return PersonModel.findOne({ email }).exec()
}

module.exports = {
    async getPeople() {
        console.log('args')
    },
    async addPerson(root, { person }) {

        const validEmail = await compareEmail(person.email)
        console.log(validEmail)

        const password = await toHashPassword(person.password)
        person.password = password
        const p = new PersonModel({...person})
        //return await p.save()
    },
}
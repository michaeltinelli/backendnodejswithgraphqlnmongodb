const PersonModel = require('../models/Person')
const { toHashPassword } = require('../config/HandlePassword')

module.exports = {
    async getPeople() {
        console.log('args')
    },
    async addPerson(root, { person }) {
        const password = await toHashPassword(person.password)
        person.password = password
        const p = new PersonModel({...person})
        return await p.save()
    }
}
const PersonModel = require('../models/Person')
const { toHashPassword } = require('../config/HandlePassword')


async function compareEmail(email) {
    return await PersonModel.findOne({ email }).select({ email }) 
}

module.exports = {
    async getPeople() {
        console.log('args')
    },
    async addPerson(root, { person }) {

        const validEmail = await compareEmail(person.email)
        if(!validEmail || validEmail == null) {
            return { message: 'Email inválido ou em uso. '}
        }

        const password = await toHashPassword(person.password)
        person.password = password
        const p = new PersonModel({...person})
        //return await p.save()
    },
}
const ProfileModel = require('../models/Profile')

async function validProfile(name) {
    return await ProfileModel.findOne({ name }).select("_id")
}

module.exports =  {

    async addProfile(root, { input }) {
        input.name = input.name.toUpperCase()
        const _id = await validProfile(input.name)
        console.log(_id)

        if(!_id) {
            const profile = new ProfileModel({ ...input })
            const resp = await profile.save()
            //console.warn(resp)
            return resp        
        } else {
            return new Error("Nome já em uso ou inválido.")
        }
    },
    async getProfiles() {
        return await ProfileModel.find().exec()
    },
    async getProfile(root, { filter }) {
        return await ProfileModel.findById({ _id: filter._id }).exec()
    },
    //não funcionando
    async removeProfile(root, args) {
        return await ProfileModel.findOneAndDelete({...args}).exec()
    },
    async updateProfile(root, { name, _id }) {
        
        await ProfileModel.updateOne({ _id }, { name }).exec()
        return await ProfileModel.findById(_id)
    }
    
}
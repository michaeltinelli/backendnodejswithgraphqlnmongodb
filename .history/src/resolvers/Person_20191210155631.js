const PersonModel = require('../models/Person')
const { toHashPassword } = require('../config/handlePassword')
const { compareEmail, getPersonByFilter } = require('../functions/person/index')
const { compareProfile } = require('../functions/profile/index')

module.exports = {
    async addPerson(root, { input }) {
        
        const validEmail = await compareEmail(input.email)
        
        if(!validEmail) {   
            const password = await toHashPassword(input.password)
            input.password = password

            const { _id } = await compareProfile("common")
            if(!_id) return new Error("Perfil não encontrado")
            else {
                const person = await new PersonModel({...input, profiles: _id})
                return person.save()
            }
        } else {
            return new Error('Erro ao cadastrar.')
        }
    },
    async updatePerson(root, { input, filter }) {
        const person = await PersonModel.findOneAndUpdate({...filter}, { ...input}).exec()
        if(!person) return new Error("Filtros não encontrados.")
        else {
            return await PersonModel.findById(person._id).exec()
        }
    },
    async removePerson(root, { filter }) {
        const resp = await PersonModel.findOneAndRemove({ ...filter }).exec()

        if(resp == null) {
            return new Error("Filtros não encontrados.")
        } else {
            return { message: "Removído com sucesso." }
        }
    }
}
const PersonModel = require('../models/Person')
const ProfileModel = require('../models/Profile')

const { toHashPassword } = require('../config/HandlePassword')

async function compareEmail(email) {
    return await PersonModel.findOne({ email }).select({ email })
}


module.exports = {
    async getPeople() {
        console.log('args')
    },
    async addPerson(root, { input }, context) {
        
        const validEmail = await compareEmail(input.email)
        
        if(!validEmail) {
           
            const password = await toHashPassword(input.password)
            input.password = password

            const p = new PersonModel({...input, profiles: [{ name: "COMMON" }]})
            return await p.save()
        } else {
            return new Error('Erro ao cadastrar.')
        }
    },
    async updatePerson(root, { newsData, filter }) {
        const resp = await PersonModel.findOneAndUpdate({ ...filter}, { ...newsData }).select()
        //console.warn(resp)
        if(resp == null) {
            return new Error("Filtros não encontrados.")
        } else {
            return PersonModel.findById(resp.id).exec()
        }
    },
    async removePerson(root, { filter }) {
        const resp = await PersonModel.findOneAndRemove({ ...filter }).exec()
        //console.log(resp)

        if(resp == null) {
            return new Error("Filtros não encontrados.")
        } else {
            return { message: "Removído com sucesso." }
        }
    }
}
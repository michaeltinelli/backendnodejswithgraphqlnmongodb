const ProfileModel = require('../models/Profile')
const { compareProfile } = require('../functions/profile/index')

module.exports =  {

    async addProfile(root, { input }) {
        input.name = input.name.toUpperCase()
        const prof = await compareProfile({...input})

        if(!prof) {
            const profile = new ProfileModel({ ...input })
            const resp = await profile.save()
            //console.warn(resp)
            return resp        
        } else {
            return new Error("Nome já em uso ou inválido.")
        }
    },
    async updateProfile(root, { filter, input }) {
        input.name = input.name.toUpperCase()
        const resp = await ProfileModel.findOneAndUpdate({...filter }, { ...input }).select()
        return resp != null ? await ProfileModel.findById(resp._id).exec() : new Error("Perfil não encontrado.")
    }
    
}
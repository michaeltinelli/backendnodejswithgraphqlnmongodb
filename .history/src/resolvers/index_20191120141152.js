const Mutations = require('./Mutations')
const Query = require('./Query')

module.exports = {
    Mutations,
    Query
}
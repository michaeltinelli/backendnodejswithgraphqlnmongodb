const ProfileModel = require('../models/Profile')

module.exports =  {

    async addProfile(root, { name }) {
        const profile = new ProfileModel({ name })
        const resp = await profile.save()
        console.warn(resp)
        return resp        
    },
    
}
const PhotoModel = require('../models/Photo')
const PersonModel = require('../models/Person')

module.exports = {
    async savePhotoOfProduct(root, { filterProduct, filterPerson, input }) {
        const person = await PersonModel.findOne({ ...filterPerson }).select()

        if(!person) return new Error("Operação Inválida.")
        else {
            const { products } = person
            const prod = products.find({...filterProduct})
            if(!prod) return new Error("Operação Inválida.")
            else {
                console.log(prod)

            }
        }
    }
}
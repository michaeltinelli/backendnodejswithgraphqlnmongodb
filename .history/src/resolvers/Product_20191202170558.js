const PersonModel = require('../models/Person')

module.exports = {
    async addHisProduct(root, { input }) {
        const user = await PersonModel.findById(input._idUser).select("products")
        //console.warn(user)
       if(!user) {
           return new Error("Usuário não encontrado")
       } else {
            delete input._idUser
            const { _id } = user
            user.products.push({ ...input, createdAt: new Date() })
            const resp = await PersonModel.findByIdAndUpdate({ _id }, { ...user }).exec()
            console.warn(resp)
       }
    },
    async getHisProducts() {
        
    },
    async updateHisProduct(root, { product, filter }) {
        
    }
}
const PersonModel = require('../models/Person')

module.exports = {
    async addHisProduct(root, { input }) {
       const { products } = await PersonModel.findById(input._idUser).select("products")
       if(!products) {
           return new Error("Usuário não encontrado")
       }
       console.log(products)
    },
    async getHisProducts() {
        
    },
    async updateHisProduct(root, { product, filter }) {
        
    }
}
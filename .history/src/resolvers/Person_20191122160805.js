const PersonModel = require('../models/Person')

module.exports = {
    async getPeople() {
        console.log('args')
    },
    async addPerson(root, { person }) {
        
       person.profiles = JSON.parse(JSON.stringify(person.profiles));
        
        const p = new PersonModel({ ...person})
        return await p.save()
    }
}
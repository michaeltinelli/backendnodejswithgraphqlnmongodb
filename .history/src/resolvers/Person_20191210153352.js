const PersonModel = require('../models/Person')
const { toHashPassword } = require('../config/handlePassword')
const { compareEmail } = require('../functions/person/index')
const { compareProfile } = require('../functions/profile/index')

module.exports = {
    async addPerson(root, { input }) {
        
        const validEmail = await compareEmail(input.email)
        
        if(!validEmail) {   
            const password = await toHashPassword(input.password)
            input.password = password
            const person = await new PersonModel({...input})
            return await person.save(async (err, doc) => {
                if(err) return new Error("Erro ao cadastrar.")
                const { _id } = await compareProfile("common")
                doc.profiles.push(_id)
                return doc
            })

        } else {
            return new Error('Erro ao cadastrar.')
        }
    },
    async updatePerson(root, { newsData, filter }) {
        
    },
    async removePerson(root, { filter }) {
        const resp = await PersonModel.findOneAndRemove({ ...filter }).exec()

        if(resp == null) {
            return new Error("Filtros não encontrados.")
        } else {
            return { message: "Removído com sucesso." }
        }
    }
}
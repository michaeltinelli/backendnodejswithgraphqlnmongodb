const PersonModel = require('../models/Person')

module.exports = {
    async addHisProduct(root, { input }) {
        const user = await PersonModel.findById(input._idUser).select("products")
        //console.warn(user)
       if(!user) {
           return new Error("Usuário não encontrado")
       } else {
            delete input._idUser
            const { _id, products } = user
            products.push({ ...input, createdAt: new Date() })
            const resp = await PersonModel.findByIdAndUpdate({ _id }, { products }).exec()
            
            if(!resp) return new Error("Erro ao inserir um novo produto.")
            else {
                const userUpdated = await PersonModel.findById({ _id}).select("products")
                return userUpdated.products.pop()
            }
       }
    },
    async getHisProducts(root, { filter }) {
        const resp = await PersonModel.findOne({ ...filter}).select("products")
        if(!resp) return new Error("Erro ao listar os produtos")
        else {
            return resp.products
        }
    },
    async updateOneOfHisProducts(root, { product, filterPerson, filterProduct }) {
        const resp = await PersonModel.findOne({ ...filterPerson }).select("products")
        
        if(!resp) new Error("Filtros inválidos")
        else {
            const { products } = resp
            const prod = products.find(p => p._id == filterProduct._id)
            
            const updatedProd = {
                _id: prod._id,
                ...product,
                updatedAt: new Date()
            }
            
            const user = await PersonModel.findOneAndUpdate({ ...filterPerson }, { products }).exec()
            
            if(!user) return new Error("Erro ao atualizar o produto.")
            else {
                const { _id } = user
                const userUpdated = await PersonModel.findById({ _id}).select("products")
                //return userUpdated.products.filter({...filterProduct})
            }

        }
    }
}
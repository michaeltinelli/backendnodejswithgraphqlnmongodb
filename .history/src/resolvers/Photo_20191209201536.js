const PhotoModel = require('../models/Photo')
const PersonModel = require('../models/Person')

module.exports = {
    async savePhotoOfProduct(root, { filterProduct, filterPerson, input }) {
        const person = await PersonModel.findOne({ ...filterPerson }).select()
        
        if(!person) return new Error("Operação Inválida.")
        else {
            const { products } = person
            
            let prod = products.find(p => p._id == filterProduct._id)
            if(!prod) return new Error("Operação Inválida.")
            else {
                await prod.photos.push({...input})
                await PersonModel.findOneAndUpdate({...filterPerson}, { products }).exec()
                return { message: "Foto adicionado com sucesso." }
            }
        }
    },
    async removePhotoOfProduct(root, { filterProduct, filterPerson, filterPhoto }) {
        const person = await PersonModel.findOne({ ...filterPerson }).select()
        
        if(!person) return new Error("Operação Inválida.")
        else {
            const { products } = person
            
            let prod = products.find(p => p._id == filterProduct._id)
            if(!prod) return new Error("Operação Inválida.")
            else {
                const { photos } = prod
                await photos.filter(p => p._id == filterPhoto._id).shift()
                console.warn(photos)
                return { message: "Foto removida com sucesso." }
            }
        }
    }
}
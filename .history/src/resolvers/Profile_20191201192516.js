const ProfileModel = require('../models/Profile')

module.exports =  {

    async addProfile(root, { input }) {
        const profile = new ProfileModel({ ...input })
        const resp = await profile.save()
        console.warn(resp)
        return resp        
    },
    async getProfiles() {
        return await ProfileModel.find().exec()
    },
    async getProfile(root, { filter }) {
        return await ProfileModel.findById({ _id: filter._id }).exec()
    },
    //não funcionando
    async removeProfile(root, args) {
        return await ProfileModel.findOneAndDelete({...args}).exec()
    },
    async updateProfile(root, { name, _id }) {
        
        await ProfileModel.updateOne({ _id }, { name }).exec()
        return await ProfileModel.findById(_id)
    }
    
}
const PersonModel = require('../models/Person')
const { toHashPassword } = require('../config/HandlePassword')


async function compareEmail(email) {
    return await PersonModel.findOne({ email }).select({ email })
}

module.exports = {
    async getPeople() {
        console.log('args')
    },
    async addPerson(root, { person }) {
        
        const validEmail = await compareEmail(person.email)
        
        if(validEmail == null) {
            const password = await toHashPassword(person.password)
            person.password = password
            const p = new PersonModel({...person})
            return await p.save()
        } else {
            return new Error('Email inválido ou já em uso. ')
        }

    },
    async updatePerson(root, { newsData, filter }) {
        const resp = await PersonModel.findOneAndUpdate({ ...filter}, { ...newsData }).select()
        //console.warn(resp)
        if(resp == null) {
            return new Error("Filtros não encontrados.")
        } else {
            return PersonModel.findById(resp.id).exec()
        }
    },
    async removePerson(root, { filter }) {
        const resp = await PersonModel.findOneAndRemove({ ...filter }).exec()
        console.log(resp)

        if(resp == null) {
            return new Error("Filtros não encontrados.")
        } else {
            return { message: "Removído com sucesso." }
        }
    }
}
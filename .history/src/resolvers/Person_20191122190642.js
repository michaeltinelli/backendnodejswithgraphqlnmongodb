const PersonModel = require('../models/Person')
const { toHashPassword } = require('../config/HandlePassword')

module.exports = {
    async getPeople() {
        console.log('args')
    },
    async addPerson(root, { person }) {

        const validEmail = await this.compareEmail(person.email)
        console.log(validEmail)

        const password = await toHashPassword(person.password)
        person.password = password
        const p = new PersonModel({...person})
        //return await p.save()
    },
    async compareEmail(email) {
        return await PersonModel.findOne({ email })
    }
}
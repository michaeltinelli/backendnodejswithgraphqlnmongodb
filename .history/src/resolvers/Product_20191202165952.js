const PersonModel = require('../models/Person')

module.exports = {
    async addHisProduct(root, { input }) {
        const user = await PersonModel.findById(input._idUser).select()
        console.warn(user)
       if(!user) {
           return new Error("Usuário não encontrado")
       } else {
            delete input._idUser
            user.products.push({ ...input, createdAt: new Date() })
            const resp = await PersonModel.findByIdAndUpdate(user._id, { ...user }).exec()
            console.warn(resp)
       }
    },
    async getHisProducts() {
        
    },
    async updateHisProduct(root, { product, filter }) {
        
    }
}
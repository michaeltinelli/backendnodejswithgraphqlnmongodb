const PersonModel = require('../models/Person')

module.exports = {
    async addHisProduct(root, { input, filterPerson }) {
        const user = await PersonModel.findOne({...filterPerson}).select()
        //console.warn(user)

        if(!user) {
            return new Error("Usuário não encontrado")
        } else {
                const { _id, products } = user
                products.push({ ...input, createdAt: new Date() })
                const resp = await PersonModel.findByIdAndUpdate({ _id }, { products }).exec()
                
                if(!resp) return new Error("Erro ao inserir um novo produto.")
                else {
                    const userUpdated = await PersonModel.findById({ _id}).select("products")
                    return userUpdated.products.pop()
                }
        }
    },
    async getHisProducts(root, { filter }) {
        const resp = await PersonModel.findOne({ ...filter}).select("products")
        if(!resp) return new Error("Erro ao listar os produtos")
        else {
            return resp.products
        }
    },
    //incompleto
    async updateOneOfHisProducts(root, { product, filterPerson, filterProduct }) {
        const person = await PersonModel.findOne({ ...filterPerson }).select("products")
       

        if(!person) new Error("Filtros inválidos")
        else {
            const { products } = person
            const prod = products.find(p => p._id == filterProduct._id)
            if(!prod) new Error("Filtros inválidos")
            else {
                const prodUpdated = {
                    ...prod,
                    ...product,
                    updatedAt: new Date()
                }
                products.find(p => p._id == filterProduct._id).splice(0,1, prodUpdated)
                const user = await PersonModel.findOneAndUpdate({ ...filterPerson }, { products }).exec()
                
                if(!user) return new Error("Erro ao atualizar o produto.")
                else {
                    return user.products.find(p => p._id == filterProduct._id)
                }
            }
        }
    },
    async removeOneOfHisProduct(root, { filterPerson, filterProduct }) {
        const person = await PersonModel.findOne({ ...filterPerson }).select("products")
       
        if(!person) new Error("Filtros inválidos")
        else { 
            let { products } = person
            const prods = await products.filter(p => p._id != filterProduct._id)
            await PersonModel.findOneAndUpdate({ ...filterPerson }, { products: prods }).exec()
            return { message: "Produto removído com sucesso." }
        }
    }
}
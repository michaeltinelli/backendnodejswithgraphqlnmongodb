const ProductModel = require('../models/Product')
const PersonModel = require('../models/Person')

module.exports = {
    async addProduct(root, { email, product }) {
        const prods =  await PersonModel.findOneAndUpdate({ email }).select({ products })
        const prod = new ProductModel({ ...product, createdAt: new Date() })
        prods.save(prod)
    },
}
const PersonModel = require('../models/Person')

module.exports = {
    async getPeople() {
        console.log('args')
    },
    async addPerson(root, { person }) {
        let { profiles } = person
        profiles = JSON.parse(JSON.stringify(profiles));
        person = profiles
        const p = new PersonModel({ ...person})
        return await p.save()
    }
}
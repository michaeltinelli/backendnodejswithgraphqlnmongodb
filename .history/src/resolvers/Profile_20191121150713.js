const ProfileModel = require('../models/Profile')

module.exports =  {

    async addProfile(root, { name }) {
        const profile = new ProfileModel({ name })
        const resp = await profile.save()
        console.warn(resp)
        return resp        
    },
    async getProfiles() {
        return await ProfileModel.find().exec()
    },
    async getProfile(root, { _id }) {
        return await ProfileModel.findById(_id).exec() || { message: `Não encontrado.` }
    },
    async removeProfile(root, args) {
        return await ProfileModel.find({ ...args }).deleteOne()
    }
    
}
const ProductModel = require('../models/Product')

module.exports = {
    async addProduct(root, { input }) {
        const prod = new ProductModel({ ...input, createdAt: new Date() })
        return await prod.save()
    },
    async getProducts() {
        return await ProductModel.find().exec()
    },
    async updateProduct(root, { product, filter }) {
        const resp = await ProductModel.findOneAndUpdate({ ...filter }, { ...product, updatedAt: new Date() })
        console.log(resp)
    }
}
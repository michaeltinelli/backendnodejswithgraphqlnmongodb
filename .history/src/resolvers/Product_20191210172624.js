const PersonModel = require('../models/Person')
const ProductModel = require('../models/Product')
const { getPerson } = require("../functions/person/index")

module.exports = {
    async addProduct(root, { input, filterPerson }) {
        const person = await getPerson(filterPerson)
        
        if(!person) return new Error("Filtros inválidos.")
        else {
            const { _id } = person
            const prod = new ProductModel({...input, person_id: _id })
            return prod.save()
        }
    },
    async getProductsOfPerson(root, { filterPerson }) {
        const _id = await getPerson(filterPerson)
        console.log(_id)
        if(_id) return new Error("Filtros inválidos")
        else {
            return await ProductModel.find({ person_id: _id })
        }
    },
    async updateProduct(root, { product, filterPerson, filterProduct }) {
        
        
    },
    async removeProduct(root, { filterPerson, filterProduct }) {
        
    }
}
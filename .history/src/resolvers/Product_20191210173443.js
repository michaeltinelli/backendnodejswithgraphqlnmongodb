const PersonModel = require('../models/Person')
const ProductModel = require('../models/Product')
const { getPerson } = require("../functions/person/index")

module.exports = {
    async addProduct(root, { input, filterPerson }) {
        const person = await getPerson(filterPerson)
        
        if(!person) return new Error("Filtros inválidos.")
        else {
            const { _id } = person
            const prod = new ProductModel({...input, person_id: _id })
            return prod.save()
        }
    },
    async getProductsOfPerson(root, { filterPerson }) {
        const person = await getPerson(filterPerson)
        if(!person) return new Error("Filtros inválidos.")

        else {
            const { _id } = person
            return await ProductModel.find({ person_id: _id })
        }
    },
    async updateProduct(root, { product, filterPerson, filterProduct }) {
        
        
    },
    async removeProduct(root, { filterPerson, filterProduct }) {
        
    }
}
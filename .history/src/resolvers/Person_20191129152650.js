const PersonModel = require('../models/Person')
const ProfileModel = require('../models/Profile')

const { toHashPassword } = require('../config/HandlePassword')

async function compareEmail(email) {
    return await PersonModel.findOne({ email }).select({ email })
}

async function validProfile(name) {
    console.log(name)
    const profiles = await ProfileModel.find()
    return profiles.includes(name) ? true : false
}

module.exports = {
    async getPeople() {
        console.log('args')
    },
    async addPerson(root, { input }) {
        const validEmail = await compareEmail(input.email)
        
        if(validEmail == null) {
            const password = await toHashPassword(input.password)
            input.password = password
            const { types } = input

            types.forEach(t => {
                if(!validProfile(t.name)) {
                    return new Error("Erro ao cadastrar. Perfil inválido.")
                }
            })

            //const p = new PersonModel({...person})
            //return await p.save()
        } else {
            return new Error('Email inválido ou já em uso. ')
        }

    },
    async updatePerson(root, { newsData, filter }) {
        const resp = await PersonModel.findOneAndUpdate({ ...filter}, { ...newsData }).select()
        //console.warn(resp)
        if(resp == null) {
            return new Error("Filtros não encontrados.")
        } else {
            return PersonModel.findById(resp.id).exec()
        }
    },
    async removePerson(root, { filter }) {
        const resp = await PersonModel.findOneAndRemove({ ...filter }).exec()
        console.log(resp)

        if(resp == null) {
            return new Error("Filtros não encontrados.")
        } else {
            return { message: "Removído com sucesso." }
        }
    }
}
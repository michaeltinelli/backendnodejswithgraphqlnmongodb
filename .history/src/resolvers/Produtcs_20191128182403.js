const ProductModel = require('../models/Product')

module.exports = {
    async addProduct(root, { product }) {
        const prod =  new ProductModel({ ...product })
        return await prod.save()
    }
}
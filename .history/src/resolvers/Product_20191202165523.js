const PersonModel = require('../models/Person')

module.exports = {
    async addHisProduct(root, { input }) {
       const user = await PersonModel.findById(input._idUser).select()

       if(!user) {
           return new Error("Usuário não encontrado")
       } else {
            delete input._idUser
            const { products } = user
            products.push({ ...input, createdAt: new Date() })
       }
    },
    async getHisProducts() {
        
    },
    async updateHisProduct(root, { product, filter }) {
        
    }
}
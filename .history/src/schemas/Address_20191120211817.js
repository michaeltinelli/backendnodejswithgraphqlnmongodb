const {
    GraphQLString,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLInt
} = require("graphql");

const AddressType = new GraphQLObjectType({
    name: 'Address',
    fields: () =>  ({
        name: { type: new GraphQLNonNull(GraphQLString) },
        comp: { type: GraphQLString },
        cep: { type: new GraphQLNonNull(GraphQLInt) },
        num: { type: new GraphQLNonNull(GraphQLInt) }
    })
})

module.exports = {
    AddressType
}
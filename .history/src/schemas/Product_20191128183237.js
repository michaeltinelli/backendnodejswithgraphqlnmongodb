const {
    GraphQLID,
    GraphQLString,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLInt,
    GraphQLFloat,
    
} = require("graphql");


const ProductType = new GraphQLObjectType({
    name: 'Product',
    fields: () =>  ({
        _id: {  type: GraphQLID },
        name: { type: GraphQLString },
        price: { type: GraphQLFloat },
        createdAt: { type: GraphQLObjectType },
        updatedAt: { type: GraphQLObjectType },
    })
})

module.exports = ProductType
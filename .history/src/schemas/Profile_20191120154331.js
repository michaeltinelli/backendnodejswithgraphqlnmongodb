const {
    GraphQLID,
    GraphQLString,
    GraphQLNonNull,
    GraphQLObjectType,
} = require("graphql");

export const ProfileType = new GraphQLObjectType({
    name: 'Profile',
    fields: () =>  ({
        _id: {  type: new GraphQLNonNull(GraphQLID) },
        name: { type: new GraphQLNonNull(GraphQLString) },
    })
})
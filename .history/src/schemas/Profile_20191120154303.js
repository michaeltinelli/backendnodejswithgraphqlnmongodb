const {
    GraphQLID,
    GraphQLString,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLEnumType,
    GraphQLInt
} = require("graphql");

export const ProfileType = new GraphQLObjectType({
    name: 'Profile',
    fields: () =>  ({
        _id: {  type: new GraphQLNonNull(GraphQLID) },
        name: { type: new GraphQLNonNull(GraphQLString) },
    })
})
const {
    GraphQLID,
    GraphQLString,
    GraphQLNonNull,
    GraphQLObjectType,
} = require("graphql");

const ProfileType = new GraphQLObjectType({
    name: 'Profile',
    fields: () =>  ({
        _id: {  type: new GraphQLNonNull(GraphQLID) },
        name: { type: new GraphQLNonNull(GraphQLString) },
    })
})

module.exports = {
    ProfileType
}
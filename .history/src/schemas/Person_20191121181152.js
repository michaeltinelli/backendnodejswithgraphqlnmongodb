const {
    GraphQLID,
    GraphQLString,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLInt,
} = require("graphql");

const AddressType = require('./Address')
const ProfileType = require('./Profile')

const PersonType = new GraphQLObjectType({
    name: 'Person',
    fields: () =>  ({
        _id: {  type: (GraphQLID) },
        name: { type: (GraphQLString) },
        last_name: { type: (GraphQLString) },
        email: { type: (GraphQLString) },
        password: { type: (GraphQLString) },
        phone: { type: (GraphQLInt) },
        profiles: { type: new GraphQLList(ProfileType) },
        address: { type:  AddressType },
    })
})

module.exports = {
    PersonType
}
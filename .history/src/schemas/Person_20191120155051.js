const {
    GraphQLID,
    GraphQLString,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLInt
} = require("graphql");

export const AddressType = new GraphQLObjectType({
    name: 'Address',
    fields: () =>  ({
        name: { type: new GraphQLNonNull(GraphQLString) },
        num: { type: new GraphQLNonNull(GraphQLInt) },
        comp: { type: GraphQLString },
        cep: { type: new GraphQLNonNull(GraphQLInt) },
    })
})
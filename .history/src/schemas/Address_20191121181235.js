const {
    GraphQLString,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLInt
} = require("graphql");

const AddressType = new GraphQLObjectType({
    name: 'Address',
    fields: () =>  ({
        name: { type: GraphQLString },
        comp: { type: GraphQLString },
        cep: { type: GraphQLInt },
        num: { type: GraphQLInt }
    })
})

module.exports = {
    AddressType
}
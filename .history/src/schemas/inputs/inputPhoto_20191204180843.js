const { GraphQLInputObjectType, GraphQLNonNull, GraphQLString } = require('graphql')

const InputPhoto = new GraphQLInputObjectType({
    name: 'InputPhoto',
    fields: () => ({
        fullPath: { type: new GraphQLNonNull(GraphQLString) },
    })
})

const FilterPhoto = new GraphQLInputObjectType({
    name: 'FilterPhoto',
    fields: () => ({
        _idProduct: { type: GraphQLString },
        _idUser: { type: GraphQLString },
    })
})

module.exports = { InputPhoto, FilterPhoto}
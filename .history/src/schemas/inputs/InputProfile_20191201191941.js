const { GraphQLInputObjectType, GraphQLNonNull, GraphQLString } = require('graphql')

const InputProfile = new GraphQLInputObjectType({
    name: 'InputProfile',
    fields: () => ({
        name: { type: new GraphQLNonNull(GraphQLString) },
    })
})

const FilterProfile = new GraphQLInputObjectType({
    name: 'FilterProfile',
    fields: () => ({
        _id: { type: GraphQLString },
        name: { type: GraphQLString },
    })
})

module.exports = { InputProfile, FilterProfile}
const { GraphQLInputObjectType, GraphQLNonNull, 
    GraphQLString, GraphQLList, GraphQLInt } = require('graphql')

const InputProduct = new GraphQLInputObjectType({
    name: 'InputProduct',
    fields: () => ({
        name: { type: new GraphQLNonNull(GraphQLString) },
        price: { type: new GraphQLNonNull(GraphQLFloat) },
    })
})


module.exports = { InputProduct, }
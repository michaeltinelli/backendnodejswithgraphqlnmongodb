const { GraphQLInputObjectType, GraphQLNonNull, 
    GraphQLString, GraphQLFloat, GraphQLObjectType } = require('graphql')

    const InputProduct = new GraphQLInputObjectType({
        name: 'InputProduct',
        fields: () => ({
            name: { type: new GraphQLNonNull(GraphQLString) },
            price: { type: new GraphQLNonNull(GraphQLFloat) },
            _idUser: {type:  GraphQLString}
        })
    })
    

module.exports = { InputProduct, }
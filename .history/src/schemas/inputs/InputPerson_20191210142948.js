const { GraphQLInputObjectType, GraphQLNonNull, GraphQLString, 
GraphQLList, GraphQLInt, GraphQLEnumType } = require('graphql')
const InputAddress = require('./InputAddress')

const InputPerson = new GraphQLInputObjectType({
    name: 'InputPerson',
    fields: () => ({
        name: { type: new GraphQLNonNull(GraphQLString) },
        last_name: { type: new GraphQLNonNull(GraphQLString) },
        phone: { type: new GraphQLNonNull(GraphQLString) },
        email: { type: new GraphQLNonNull(GraphQLString) },
        password: { type: new GraphQLNonNull(GraphQLString) },
        address: { type: InputAddress },
        profiles: { type: new GraphQLNonNull(GraphQLString) },
    })
})

const UpdatePerson = new GraphQLInputObjectType({
    name: 'UpdatePerson',
    fields: () => ({
        name: { type: GraphQLString },
        last_name: { type: GraphQLString },
        phone: { type: GraphQLString},
        address: { type: InputAddress }
    })
})

const FilterPerson = new GraphQLInputObjectType({
    name: 'FilterPerson',
    fields: () => ({
        _id: { type: GraphQLString },
        email: { type: GraphQLString },
    })
})

module.exports = { InputPerson, UpdatePerson, FilterPerson }
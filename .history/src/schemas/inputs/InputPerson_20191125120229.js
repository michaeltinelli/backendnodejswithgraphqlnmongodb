const { GraphQLInputObjectType, GraphQLNonNull, GraphQLString, GraphQLList } = require('graphql')
const InputProfile = require('./InputProfile')
const InputAddress = require('./InputAddress')

const InputPerson = new GraphQLInputObjectType({
    name: 'InputPerson',
    fields: () => ({
        name: { type: new GraphQLNonNull(GraphQLString) },
        last_name: { type: new GraphQLNonNull(GraphQLString) },
        phone: { type: new GraphQLNonNull(GraphQLString) },
        email: { type: new GraphQLNonNull(GraphQLString) },
        password: { type: new GraphQLNonNull(GraphQLString) },
        profiles: { type: new GraphQLNonNull( new GraphQLList(InputProfile)) },
        address: { type: InputAddress }
    })
})

const UpdatePerson = new GraphQLInputObjectType({
    name: 'UpdatePerson',
    fields: () => ({
        name: { type: new GraphQLNonNull(GraphQLString) },
        last_name: { type: new GraphQLNonNull(GraphQLString) },
        phone: { type: new GraphQLNonNull(GraphQLString) },
        address: { type: InputAddress }
    })
})

const ArgsUpdatePerson = new GraphQLInputObjectType({
    name: 'ArgsUpdatePerson',
    fields: () => ({
        id: { type: GraphQLString },
        email: { type: GraphQLString },
    })
})

module.exports = { InputPerson, UpdatePerson, ArgsUpdatePerson }
const { GraphQLInputObjectType, GraphQLNonNull, GraphQLString } = require('graphql')

const InputProfile = new GraphQLInputObjectType({
    name: 'InputProfile',
    fields: () => ({
        name: { type: new GraphQLNonNull(GraphQLString) },
    })
})

module.exports = InputProfile
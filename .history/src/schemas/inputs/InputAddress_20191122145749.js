const { GraphQLInputObjectType, GraphQLNonNull, GraphQLString, GraphQLInt } = require('graphql')

const InputAdress = new GraphQLInputObjectType({
    name: 'InputAdress',
    fields: () => ({
        name: { type: new GraphQLNonNull(GraphQLString) },
        comp: { type: GraphQLString },
        cep: { type: new GraphQLNonNull(GraphQLString) },
        num: { type: new GraphQLNonNull(GraphQLInt) },
    })
})

module.exports = InputAdress
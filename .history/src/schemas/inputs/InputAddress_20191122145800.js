const { GraphQLInputObjectType, GraphQLNonNull, GraphQLString, GraphQLInt } = require('graphql')

const InputAddress = new GraphQLInputObjectType({
    name: 'InputAddress',
    fields: () => ({
        name: { type: new GraphQLNonNull(GraphQLString) },
        comp: { type: GraphQLString },
        cep: { type: new GraphQLNonNull(GraphQLString) },
        num: { type: new GraphQLNonNull(GraphQLInt) },
    })
})

module.exports = InputAddress
const { GraphQLInputObjectType, GraphQLNonNull, GraphQLString } = require('graphql')

const InputPerson = new GraphQLInputObjectType({
    name: 'InputPerson',
    fields: () => ({
        name: { type: new GraphQLNonNull(GraphQLString) },
        last_name: { type: new GraphQLNonNull(GraphQLString) },
        phone: { type: new GraphQLNonNull(GraphQLString) },
        email: { type: new GraphQLNonNull(GraphQLString) },
        password: { type: new GraphQLNonNull(GraphQLString) },
    })
})

module.exports = InputPerson
const {
    GraphQLID,
    GraphQLString,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLInt,
    GraphQLFloat,
    
} = require("graphql");


const ProductType = new GraphQLObjectType({
    name: 'Product',
    fields: () =>  ({
        _id: {  type: GraphQLID },
        name: { type: GraphQLString },
        price: { type: GraphQLFloat },
        createdAt: { type: new GraphQLObjectType },
        updatedAt: { type: new GraphQLObjectType },
    })
})

module.exports = ProductType
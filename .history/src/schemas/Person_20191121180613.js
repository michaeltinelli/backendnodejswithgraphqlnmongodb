const {
    GraphQLID,
    GraphQLString,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLInt,
} = require("graphql");

const AddressType = require('./Address')
const ProfileType = require('./Profile')

const PersonType = new GraphQLObjectType({
    name: 'Person',
    fields: {
        _id: {  type: GraphQLNonNull(GraphQLID) },
        name: { type: GraphQLNonNull(GraphQLString) },
        last_name: { type: GraphQLNonNull(GraphQLString) },
        email: { type: GraphQLNonNull(GraphQLString) },
        password: { type: GraphQLNonNull(GraphQLString) },
        phone: { type: GraphQLNonNull(GraphQLInt) },
        profiles: { type: GraphQLList(ProfileType) },
        address: { type: AddressType },
    }
})

module.exports = {
    PersonType
}
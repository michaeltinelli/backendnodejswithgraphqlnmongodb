const {
    GraphQLID,
    GraphQLString,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLInt,
} = require("graphql");

const AddressType = require('./Address')
const ProfileType = require('./Profile')

const PersonType = new GraphQLObjectType({
    name: 'Person',
    fields: () =>  ({
        _id: {  type: new GraphQLNonNull(GraphQLID) },
        name: { type: new GraphQLNonNull(GraphQLString) },
        last_name: { type: new GraphQLNonNull(GraphQLString) },
        email: { type: new GraphQLNonNull(GraphQLString) },
        password: { type: new GraphQLNonNull(GraphQLString) },
        phone: { type: new GraphQLNonNull(GraphQLInt) },
        profiles: { type: GraphQLList(ProfileType) },
        address: { type: AddressType },
    })
})

module.exports = {
    PersonType
}
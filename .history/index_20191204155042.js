require('dotenv').config()
const { username, password } = require('./.env')
const express = require('express')
const cors =  require('cors')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const ExpressGraphQL = require('express-graphql')
const mongoConnect = `mongodb+srv://${username}:${password}@cluster0-lhlvj.mongodb.net/mydebts
?retryWrites=true&w=majority
`
mongoose.connect(mongoConnect, { useUnifiedTopology: true, useNewUrlParser: true })

const schemaQM = require('./src/queryAndMutations/Profile')
const schemaPQM = require('./src/queryAndMutations/Person')

const context = require('./src/config/context')
const server = express()

server.use(express.static('public'))
server.use(bodyParser.json())
server.use(bodyParser.urlencoded({ extended: true }))
server.use(express.json())
server.use(cors())


server.use(`/profile`, ExpressGraphQL({
    schema: schemaQM,
    graphiql: true,
}))

server.use(`/person`, ExpressGraphQL({
    schema: schemaPQM,
    graphiql: true,
    context
}))



server.listen(8500, () => {
    console.log(`running`)
})